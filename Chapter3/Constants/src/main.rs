use rand::Rng;

fn main() {
    //I think contants may only be things that can be "baked in" at compile time
    //const SECRET: u32 = rand::thread_rng().gen_range(1..101);
    const SECRET: u32 = 32 * 32 * 32;
    println!("{}", SECRET);
}
