use std::io;

fn main() {
    let mut n = String::new();
    println!("Give me a non-negative number!");
    io::stdin().read_line(&mut n).expect("Failed to read line");
    let n: u64 = n
        .trim()
        .parse()
        .expect("Input must be non-negative integer");
    let fibonum = fibo(n);
    println!("The {} fibonacci number is {}.", n, fibonum);
}

fn fibo(n: u64) -> u64 {
    if n == 0 {
        0
    } else if n == 1 {
        1
    } else {
        let mut acc1 = 1;
        let mut acc2 = 1;
        for _number in 2..n {
            let together = acc1 + acc2;
            acc1 = acc2;
            acc2 = together;
        }
        acc2
    }
}
