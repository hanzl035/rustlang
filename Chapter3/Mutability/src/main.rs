fn main() {
 let x = 5;
 let x = 6; // shadowing will not throw an error
 println!("{}", x);
 x = 5; // trying to change an immutable will throw an error
}
