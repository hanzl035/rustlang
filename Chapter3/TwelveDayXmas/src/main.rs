fn main() {
    const NUMBER_TENSES: [&str; 12] = [
        "On the first day of Christmas",
        "On the second day of Christmas",
        "On the third day of Christmas",
        "On the fourth day of Christmas",
        "On the fifth day of Christmas",
        "On the sixth day of Christmas",
        "On the seventh day of Christmas",
        "On the eighth day of Christmas",
        "On the ninth day of Christmas",
        "On the tenth day of Christmas",
        "On the eleventh day of Christmas",
        "On the twelfth day of Christmas",
    ];
    const LOVE: &str = "My true love gave to me";
    const FIRST_PARTRIDGE: &str = "A partridge in a pear tree!";
    const VERSES: [&str; 12] = [
        "And a partridge in a pear tree!",
        "Two turtle doves",
        "Three french hens",
        "Four calling birds",
        "Five golden rings!",
        "Six geese a layin'",
        "Seven swans a swimmin'",
        "Eight maids milkin'",
        "Nine ladies dancin'",
        "Ten lords a leapin'",
        "Eleven pipers pipin'",
        "Twelve drummers drummin'",
    ];
    for round in 0..12 {
        println!("{}", NUMBER_TENSES[round]);
        println!("{}", LOVE);
        if round == 0 {
            println!("{}", FIRST_PARTRIDGE)
        } else {
            for line in (0..round + 1).rev() {
                println!("{}", VERSES[line]);
            }
        }
        println!();
    }
}
