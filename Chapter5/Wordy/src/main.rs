struct Wordy {
    stringy: String,
}

impl Wordy {
    fn from(stringy: &str) -> Wordy {
        Wordy {
            stringy: stringy.to_string(),
        }
    }

    fn count(&self) -> usize {
        let bytes = self.stringy.trim().as_bytes();
        let mut count = 0;
        if bytes.len() > 0 {
            count = 1;
            for (i, &item) in bytes.iter().enumerate() {
                if item == b' ' {
                    count = count + 1;
                }
            }
        }
        count
    }
}

fn main() {
    let stringy = String::from("");
    let hello = Wordy::from(&stringy);
    let qwerty = Wordy::from("qwerty");
    println!("{}", hello.count());
    println!("{}", qwerty.count());
    let one = Wordy::from("one");
    let sone = Wordy::from(" one");
    let ones = Wordy::from("one ");
    let sones = Wordy::from(" one ");
    let onetwo = Wordy::from("one two");
    let sonetwo = Wordy::from(" one two");
    let onestwo = Wordy::from("one  two"); // this should fail due to dbl space in middle?
    let sonestwo = Wordy::from(" one  two"); // this should fail due to dbl space in middle?
    let sonetwos = Wordy::from(" one two ");
    println!("one {}", one.count());
    println!("sone {}", sone.count());
    println!("ones {}", ones.count());
    println!("sones {}", sones.count());
    println!("onetwo {}", onetwo.count());
    println!("sonetwo {}", onetwo.count());
    println!("onestwo {}", onetwo.count());
    println!("sonestwo {}", onetwo.count());
    println!("sonetwos {}", onetwo.count());
    println!("{}", sonestwo.stringy);
    println!("{}", sonestwo.stringy.trim());
    println!("{}", sonetwos.stringy);
    println!("{}", sonetwos.stringy.trim());
}
