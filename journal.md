# December 29 2021
I haven't been studying Rust for a bit, which is too bad. Last night I tried
running my old project examples on another PC (running Debian) and immediately
ran into issues. The edition of the Cargo.toml file was too advanced. Luckily,
it seems the 2018 syntax (what Debian understood) and the 2021 syntax (what
my laptop understood) are mostly compatible. I only had to switch the one line
from 2021 to 2018, and now my programs run on both computers.

I spent today practicing with Structs and reading the enums section. In the
readings I got through defining enums and using the match syntax. It seems
straight forward, though I know I'll have to reference the book for a bit while
I run into every possible error while writing a simple piece of code.

It took a whole hour to make a simple Wordy struct that held a String and
a method to count the words in sed string. The design wasn't hard (because
I was able to steal functions from the previous chapter) but I'm not used
to working with types and ownership. I spent a long time inputing differant
stringy types and seeing if they would break the struct. 
